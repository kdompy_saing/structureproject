//
// Created by Saing Kdompy on 2/11/22.
//
#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_structureapplication_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "c0151499-726e-472c-96a5-bc16345e192e";
    return env->NewStringUTF(hello.c_str());
}


//Java_com_example_structureapplication_MainActivity_stringFromJNI
//Java_com_example_structureapplication_util_Constants_stringFromJNI

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_structureapplication_util_Constants_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "https://jsonplaceholder.typicode.com/";
    return env->NewStringUTF(hello.c_str());
}