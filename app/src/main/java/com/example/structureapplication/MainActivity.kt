package com.example.structureapplication

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.structureapplication.databinding.ActivityMainBinding
import com.example.structureapplication.di.ViewModelFactory
import com.example.structureapplication.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var factory: ViewModelFactory

    private val userViewModel: UserViewModel by viewModels { factory }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Log.d("dfsdfdsfdsfdsfsd", stringFromJNI())
    }

    external fun stringFromJNI(): String

    companion object {
        // Used to load the 'structureapplication' library on application startup.
        init {
            System.loadLibrary("structureapplication")
        }
    }

}