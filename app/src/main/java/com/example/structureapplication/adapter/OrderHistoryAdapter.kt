package com.example.structureapplication.adapter

import android.view.ViewGroup
import com.example.structureapplication.R
import com.example.structureapplication.databinding.ItemTestingBinding
import com.example.structureapplication.model.PosmRequestListModel
import com.example.structureapplication.viewholder.BaseViewHolder
import com.example.structureapplication.viewholder.OrderHistoryViewHolder

class OrderHistoryAdapter(mData: List<PosmRequestListModel>) :
    BaseRecyclerAdapter<ItemTestingBinding, PosmRequestListModel>(mData) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ItemTestingBinding, PosmRequestListModel> {
        return OrderHistoryViewHolder(toBinding(parent, viewType))
    }

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_testing
    }

    override fun shouldLoadMore(): Boolean = true
}