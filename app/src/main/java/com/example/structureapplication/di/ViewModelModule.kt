package com.example.structureapplication.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.structureapplication.viewmodel.UserListViewModel
import com.example.structureapplication.viewmodel.UserViewModel
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap

@Module
@InstallIn(ActivityComponent::class)
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    //    example of bind view model factory
    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    fun bindFinancialViewModel(viewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    fun bindFinanciaUserlViewModel(viewModel: UserListViewModel): ViewModel

}