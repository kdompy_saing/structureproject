package com.example.structureapplication.extension

import android.os.Build
import android.view.DisplayCutout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import com.example.structureapplication.util.SingleLiveEvent
import com.google.android.gms.tasks.OnCanceledListener
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import java.util.concurrent.Executor

fun <T> LiveData<T>.toSingleEvent(): LiveData<T> {
    val result = SingleLiveEvent<T>()
    result.addSource(this) {
        result.value = it
    }
    return result
}

fun ViewGroup.inflate(@LayoutRes res: Int) : View {
    return LayoutInflater.from(this.context).inflate(res, this, false)
}

fun <TResult> Task<TResult>.addOnSuccessListener(
    executor: Executor,
    listener: (TResult) -> Unit
): Task<TResult> {
    return addOnSuccessListener(executor, OnSuccessListener(listener))
}

/**
 * Quality-of-life helper to allow using trailing lambda syntax for adding a failure listener to a
 * [Task].
 */
fun <TResult> Task<TResult>.addOnFailureListener(
    executor: Executor,
    listener: (Exception) -> Unit
): Task<TResult> {
    return addOnFailureListener(executor, OnFailureListener(listener))
}

/**
 * Quality-of-life helper to allow using trailing lambda syntax for adding a completion listener to
 * a [Task].
 */
fun <TResult> Task<TResult>.addOnCompleteListener(
    executor: Executor,
    listener: (Task<TResult>) -> Unit
): Task<TResult> {
    return addOnCompleteListener(executor, OnCompleteListener(listener))
}

/**
 * Quality-of-life helper to allow using trailing lambda syntax for adding a cancellation listener
 * to a [Task].
 */
fun <TResult> Task<TResult>.addOnCanceledListener(
    executor: Executor,
    listener: () -> Unit
): Task<TResult> {
    return addOnCanceledListener(executor, OnCanceledListener(listener))
}

@RequiresApi(Build.VERSION_CODES.P)
fun View.padWithDisplayCutout() {

    /** Helper method that applies padding from cutout's safe insets */
    fun doPadding(cutout: DisplayCutout) = setPadding(
        cutout.safeInsetLeft,
        cutout.safeInsetTop,
        cutout.safeInsetRight,
        cutout.safeInsetBottom
    )

    // Apply padding using the display cutout designated "safe area"
    rootWindowInsets?.displayCutout?.let { doPadding(it) }

    // Set a listener for window insets since view.rootWindowInsets may not be ready yet
    setOnApplyWindowInsetsListener { _, insets ->
        insets.displayCutout?.let { doPadding(it) }
        insets
    }
}
