package com.example.structureapplication.facedetection

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import com.example.structureapplication.R
import com.example.structureapplication.databinding.ActivityFaceDetectionBinding
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceLandmark
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class FaceDetectionActivity : AppCompatActivity() {

    private var displayId = -1
    private var lensFacing = CameraSelector.LENS_FACING_FRONT
    private var preview: Preview? = null
    private var imageAnalyser: ImageAnalysis? = null
    private var camera: Camera? = null

    private var cameraProvider: ProcessCameraProvider? = null

    private var faceAnalyserMode = FaceAnalyser.REAL_TIME
    private lateinit var cameraExecutor: ExecutorService

    private lateinit var binding: ActivityFaceDetectionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFaceDetectionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cameraExecutor = Executors.newSingleThreadExecutor()
        binding.let { binding ->
            binding.previewView.post {
                displayId = binding.previewView.display.displayId
                binding.overlayContainer.facing = CameraSelector.LENS_FACING_FRONT
                setListeners()
            }
        }
    }

    private fun setListeners() {
        binding.let { binding ->
            binding.btnDetectionMode.setOnClickListener {
                if (faceAnalyserMode == FaceAnalyser.REAL_TIME) {
                    faceAnalyserMode = FaceAnalyser.HIGH_ACCURACY
                    binding.btnDetectionMode.setImageDrawable(
                        AppCompatResources.getDrawable(this, R.drawable.ic_launcher_background)
                    )
                } else {
                    faceAnalyserMode = FaceAnalyser.REAL_TIME
                    binding.btnDetectionMode.setImageDrawable(
                        AppCompatResources.getDrawable(this, R.drawable.ic_launcher_background)
                    )
                }
                bindCameraUseCase()
            }

        }
    }

    override fun onPause() {
        super.onPause()
        cameraProvider?.unbindAll()
    }

    override fun onResume() {
        super.onResume()
        binding.previewView.post { bindCameraUseCase() }
    }

    private fun bindCameraUseCase() {
        binding.let { binding ->
            binding.overlayContainer.clear()
            val metrics = DisplayMetrics().also { binding.previewView.display.getRealMetrics(it) }
            val screenSize = Size(metrics.widthPixels, metrics.heightPixels)
            val rotation = binding.previewView.display.rotation

            val cameraSelector = CameraSelector.Builder()
                .requireLensFacing(lensFacing)
                .build()
            val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
            cameraProviderFuture.addListener({
                cameraProvider = cameraProviderFuture.get()
                preview = Preview.Builder()
                    .setTargetResolution(screenSize)
                    .setTargetRotation(rotation)
                    .build()

                preview?.setSurfaceProvider(binding.previewView.surfaceProvider)

                imageAnalyser =
                    ImageAnalysis.Builder()
                        .setTargetResolution(screenSize)
                        .setTargetRotation(rotation)
                        .build()
                        .apply {
                            setAnalyzer(
                                cameraExecutor,
                                FaceAnalyser(faceAnalyserMode) { faceList ->
//                                    createOverlays(faceList)
                                    faceList.forEach {
                                        logExtrasForTesting(it)
                                    }

                                }
                            )
                        }


                cameraProvider?.unbindAll()
                try {
                    camera = cameraProvider?.bindToLifecycle(
                        this,
                        cameraSelector,
                        preview,
                        imageAnalyser
                    )
                } catch (e: Exception) {
                    Log.e("error", "Use case binding failed", e)
                }

            }, ContextCompat.getMainExecutor(this))
        }
    }

    private fun createOverlays(faceList: MutableList<Face>) {
        binding.let { binding ->
            binding.overlayContainer.clear()
            val overlayView = FaceOverlayView(binding.overlayContainer, faceList)
            binding.overlayContainer.add(overlayView)
        }
    }

    private fun logExtrasForTesting(face: Face?) {
        if (face != null) {
            val lefEyeOpen = face.leftEyeOpenProbability
            val rightEyeOpen = face.leftEyeOpenProbability
            if (lefEyeOpen != null && rightEyeOpen != null) {
                Log.d("fdsfdsfdsfdsfsdfsfds","$lefEyeOpen $rightEyeOpen")
                if (lefEyeOpen < 0.4 || rightEyeOpen < 0.4){
                    Log.d("fdsfdsfdsfdsfsdfsfds","Blinking")
                }else{
                    Log.d("fdsfdsfdsfdsfsdfsfds","NOt Blinking")
                }
            }

            Log.v(
                "MANUAL_TESTING_LOG",
                "face bounding box: " + face.boundingBox.flattenToString()
            )
            Log.v(
                "MANUAL_TESTING_LOG",
                "face Euler Angle X: " + face.headEulerAngleX
            )
            Log.v(
                "MANUAL_TESTING_LOG",
                "face Euler Angle Y: " + face.headEulerAngleY
            )
            Log.v(
                "MANUAL_TESTING_LOG",
                "face Euler Angle Z: " + face.headEulerAngleZ
            )
            // All landmarks
            val landMarkTypes = intArrayOf(
                FaceLandmark.MOUTH_BOTTOM,
                FaceLandmark.MOUTH_RIGHT,
                FaceLandmark.MOUTH_LEFT,
                FaceLandmark.RIGHT_EYE,
                FaceLandmark.LEFT_EYE,
                FaceLandmark.RIGHT_EAR,
                FaceLandmark.LEFT_EAR,
                FaceLandmark.RIGHT_CHEEK,
                FaceLandmark.LEFT_CHEEK,
                FaceLandmark.NOSE_BASE
            )
            val landMarkTypesStrings = arrayOf(
                "MOUTH_BOTTOM",
                "MOUTH_RIGHT",
                "MOUTH_LEFT",
                "RIGHT_EYE",
                "LEFT_EYE",
                "RIGHT_EAR",
                "LEFT_EAR",
                "RIGHT_CHEEK",
                "LEFT_CHEEK",
                "NOSE_BASE"
            )
            for (i in landMarkTypes.indices) {
                val landmark = face.getLandmark(landMarkTypes[i])
                if (landmark == null) {
                    Log.v(
                        "MANUAL_TESTING_LOG",
                        "No landmark of type: " + landMarkTypesStrings[i] + " has been detected"
                    )
                } else {
                    val landmarkPosition = landmark.position
                    val landmarkPositionStr =
                        String.format(
                            Locale.US,
                            "x: %f , y: %f",
                            landmarkPosition.x,
                            landmarkPosition.y
                        )
                    Log.v(
                        "MANUAL_TESTING_LOG",
                        "Position for face landmark: " +
                                landMarkTypesStrings[i] +
                                " is :" +
                                landmarkPositionStr
                    )
                }
            }
            Log.v(
                "MANUAL_TESTING_LOG",
                "face left eye open probability: " + face.leftEyeOpenProbability
            )
            Log.v(
                "eyeOpen",
                "face right eye open probability: " + face.rightEyeOpenProbability
            )
            Log.v(
                "MANUAL_TESTING_LOG",
                "face smiling probability: " + face.smilingProbability
            )
            Log.v(
                "MANUAL_TESTING_LOG",
                "face tracking id: " + face.trackingId
            )
        }
    }

    companion object {
        fun launch(context: Context) {
            context.startActivity(Intent(context, FaceDetectionActivity::class.java))
        }
    }


}