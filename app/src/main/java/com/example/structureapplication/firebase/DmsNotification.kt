package com.example.structureapplication.firebase

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.example.structureapplication.R
import org.json.JSONObject
import java.util.*

abstract class DmsNotification {
    var context: Context? = null
    private var largeIcon: Bitmap? = null
        get() {
            if (field == null) field =
                BitmapFactory.decodeResource(context?.resources, R.mipmap.ic_launcher)
            return field
        }
    var dataJson: JSONObject? = null
    private val channelId = "DMS_NOTIFICATION_ID"
    private val smallIcon: Int
        get() = R.mipmap.ic_launcher
    private val contentText: Spanned
        get() {
            val textHtml: Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(
                    dataJson!!.optString("title", context!!.getString(R.string.app_name)),
                    Html.FROM_HTML_MODE_LEGACY
                )
            } else {
                Html.fromHtml(dataJson!!.optString("title", context!!.getString(R.string.app_name)))
            }
            return textHtml
        }
    private val contentTitle: String
        get() = context!!.resources.getString(R.string.app_name)

    @get:RequiresApi(api = Build.VERSION_CODES.O)
    val notificationChannel: NotificationChannel
        get() {
            val name = "DMS_NOTIFICATION_CHANNEL"
            val mChannel = NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_HIGH)
            mChannel.enableLights(true)
            mChannel.lightColor = R.color.purple_200
            return mChannel
        }
    val notificationBuilder: NotificationCompat.Builder
        get() {
            val builder = NotificationCompat.Builder(
                context!!, channelId
            )
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(smallIcon)
                .setLargeIcon(largeIcon)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setWhen(System.currentTimeMillis())
                .setColor(ContextCompat.getColor(context!!, R.color.purple_200))
                .setAutoCancel(true)
            val contentIntent = contentIntent
            if (contentIntent != null) {
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                val resultPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    contentIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                builder.setContentIntent(resultPendingIntent)
            }
            return builder
        }
    abstract val contentIntent: Intent?
    val notificationId: Int
        get() = UUID.randomUUID().toString().hashCode()

    companion object {
        val `package`: String
            get() = "com.chipmong.dms.firebase"

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        fun create(context: Context?, data: JSONObject, notiType: String): DmsNotification? {
            var item: DmsNotification? = null
            try {
                val classLoader = DmsNotification::class.java.classLoader
                val aClass =
                    classLoader!!.loadClass(
                        "$`package`.DmsNotification" + notiType.toUpperCase(
                            Locale.getDefault()
                        )
                    )
                item = aClass.newInstance() as DmsNotification
                item.context = context
                item.dataJson = data
            } catch (e: Exception) {
                Log.w(
                    "DmsNotification",
                    "Unknown Notification code " + data.optString("notificationType")
                )
            }
            return item
        }
    }
}