package com.example.structureapplication.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.structureapplication.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class StructureFirebaseMessagingService : FirebaseMessagingService() {
//    private lateinit var notificationManager: DmsNotificationManager
//
//    override fun onCreate() {
//        super.onCreate()
//        notificationManager = DmsNotificationManager(this)
//    }
//
//    override fun onNewToken(token: String) {
//        super.onNewToken(token)
////        if (!TextUtils.equals(token, DmsSharePreference.getInstance(this).getFirebaseToken())
////            && IOrderSharePreference.getInstance(this).isPushNotification()){
////
////            IOrderSharePreference.getInstance(this).setFirebaseToken(token)
////
////            RegisterPushRequest(this, false).execute {
////                RegisterPushRequest(this, true).execute()
////            }
////        }
//    }
//
//    override fun onMessageReceived(remoteMessage: RemoteMessage) {
//        super.onMessageReceived(remoteMessage)
//        Log.d(javaClass.simpleName, "From: " + remoteMessage.from)
//        try {
//            val data = JSONObject(remoteMessage.data["rawData"].toString())
//            Log.d(javaClass.simpleName, "Message data payload JSON: " + data.toString(1))
//            notificationManager.displayNotification(data)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val title: String? = remoteMessage.notification?.title
        val text: String? = remoteMessage.notification?.body

        val CHANNEL_ID = "HEADS_UP_NOTIFICATION"

        val channel = NotificationChannel(
            CHANNEL_ID,
            "Heads Up Notification",
            NotificationManager.IMPORTANCE_HIGH
        )
        getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
        val notification: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setAutoCancel(true)

        NotificationManagerCompat.from(this).notify(1, notification.build())


    }
}