package com.example.structureapplication.model

import com.example.structureapplication.localroom.model.MovieEntity
import com.google.gson.annotations.SerializedName

class UserResponse  {
    @SerializedName("name")
    var name = ""
    @SerializedName("email")
    var email = ""
    @SerializedName("phone")
    var phone = ""
    fun mapModel(): MovieEntity {
        return MovieEntity().apply {
            this.email = this@UserResponse.email
            this.name = this@UserResponse.name
            this.phone = this@UserResponse.phone
        }
    }
}