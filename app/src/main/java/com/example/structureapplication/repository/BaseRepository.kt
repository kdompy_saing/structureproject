package com.example.structureapplication.repository

import android.util.Log
import com.example.structureapplication.util.Resource
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

abstract class BaseRepository {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.Success(body)
            }
            Log.d("dfsddsfdsfdsf",response.message())
            return errorCode(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return errorCode(e.message ?: e.toString())
        }
    }

    private fun <T> errorCode(message: String): Resource<T> {
//        Timber.d(message)
        Log.d("dfsdfdsfdsfdsf", message)
        return Resource.Error("Network call has failed for a following reason: $message")
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("message")
        } catch (e: Exception) {
            "fsdfsdf"
        }
    }

}