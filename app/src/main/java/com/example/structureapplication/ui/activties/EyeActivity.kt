package com.example.structureapplication.ui.activties

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.structureapplication.R
import com.example.structureapplication.vision.FaceTracker
import com.example.structureapplication.vision.GraphicOverlay
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.face.Face
import com.google.android.gms.vision.face.FaceDetector
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_eye.*
import java.io.IOException

class EyeActivity : AppCompatActivity() {
    private val TAG = "GooglyEyes"

    private val RC_HANDLE_GMS = 9001

    // permission request codes need to be < 256
    private val RC_HANDLE_CAMERA_PERM = 2

    private var mCameraSource: CameraSource? = null

    //    private var mPreview: CameraSourcePreview? = null
    private var mGraphicOverlay: GraphicOverlay? = null

    private var mIsFrontFacing = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eye)
//        mPreview = findViewById<CameraSourcePreview>(R.id.preview)
        mGraphicOverlay = faceOverlay

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.


        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            createCameraSource()
        } else {
            requestCameraPermission()
        }
    }

    /**
     * Handles the requesting of the camera permission.  This includes showing a "Snack bar" message
     * of why the permission is needed then sending the request.
     */
    private fun requestCameraPermission() {
        Log.w(
            TAG,
            "Camera permission is not granted. Requesting permission"
        )
        val permissions = arrayOf(Manifest.permission.CAMERA)
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(
                this,
                permissions,
                RC_HANDLE_CAMERA_PERM
            )
            return
        }
        val thisActivity: Activity = this
        val listener = View.OnClickListener {
            ActivityCompat.requestPermissions(
                thisActivity, permissions,
                RC_HANDLE_CAMERA_PERM
            )
        }
        Snackbar.make(
            mGraphicOverlay!!, "Permission",
            Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Ok", listener)
            .show()
    }

    /**
     * Restarts the camera.
     */
    override fun onResume() {
        super.onResume()
        startCameraSource()
    }

    /**
     * Stops the camera.
     */
    override fun onPause() {
        super.onPause()
        mCameraSource?.stop()
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    override fun onDestroy() {
        super.onDestroy()
        if (mCameraSource != null) {
            mCameraSource?.release()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(
                TAG,
                "Got unexpected permission result: $requestCode"
            )
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(
                TAG,
                "Camera permission granted - initialize the camera source"
            )
            // we have permission, so create the camerasource
            createCameraSource()
            return
        }
        Log.e(
            TAG,
            "Permission not granted: results len = " + grantResults.size +
                    " Result code = " + if (grantResults.isNotEmpty()) grantResults[0] else "(empty)"
        )
        val listener =
            DialogInterface.OnClickListener { dialog, id -> finish() }
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Face Tracker sample")
            .setMessage("No camera permission")
            .setPositiveButton("Ok", listener)
            .show()
    }

    /**
     * Saves the camera facing mode, so that it can be restored after the device is rotated.
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putBoolean("IsFrontFacing", mIsFrontFacing)
    }

    /**
     * Toggles between front-facing and rear-facing modes.
     */
    private val mFlipButtonListener = View.OnClickListener {
        mIsFrontFacing = !mIsFrontFacing
        if (mCameraSource != null) {
            mCameraSource?.release()
            mCameraSource = null
        }
        createCameraSource()
        startCameraSource()
    }

    //==============================================================================================
    // Detector
    //==============================================================================================

    //==============================================================================================
    // Detector
    //==============================================================================================
    /**
     * Creates the face detector and associated processing pipeline to support either front facing
     * mode or rear facing mode.  Checks if the detector is ready to use, and displays a low storage
     * warning if it was not possible to download the face library.
     */
    private fun createFaceDetector(context: Context): FaceDetector {
        val detector = FaceDetector.Builder(context)
            .setLandmarkType(FaceDetector.ALL_LANDMARKS)
            .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
            .setTrackingEnabled(true)
            .setMode(FaceDetector.FAST_MODE)
            .setProminentFaceOnly(mIsFrontFacing)
            .setMinFaceSize(if (mIsFrontFacing) 0.35f else 0.15f)
            .build()
        val processor: Detector.Processor<Face> = if (mIsFrontFacing) {
            // For front facing mode
            val tracker: Tracker<Face> = FaceTracker(mGraphicOverlay)
            LargestFaceFocusingProcessor.Builder(detector, tracker).build()
        } else {
            // For rear facing mode, a factory is used to create per-face tracker instances.
            val factory: MultiProcessor.Factory<Face> =
                MultiProcessor.Factory { FaceTracker(mGraphicOverlay) }
            MultiProcessor.Builder(factory).build()
        }
        detector.setProcessor(processor)
        if (!detector.isOperational) {

            // isOperational() can be used to check if the required native library is currently available.  .
            Log.w(
                TAG,
                "Face detector dependencies are not yet available."
            )

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            val lowStorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = registerReceiver(null, lowStorageFilter) != null
            if (hasLowStorage) {
                Toast.makeText(this, "low_storage_error", Toast.LENGTH_LONG).show()
                Log.w(
                    TAG,
                    "low_storage_error"
                )
            }
        }
        return detector
    }


    /**
     * Creates the face detector and the camera.
     */
    private fun createCameraSource() {
        val context = applicationContext
        val detector = createFaceDetector(context)
        var facing = CameraSource.CAMERA_FACING_FRONT
        if (!mIsFrontFacing) {
            facing = CameraSource.CAMERA_FACING_BACK
        }
        mCameraSource = CameraSource.Builder(context, detector)
            .setFacing(facing)
            .setRequestedPreviewSize(320, 240)
            .setRequestedFps(60.0f)
            .setAutoFocusEnabled(true)
            .build()
    }


    private fun startCameraSource() {
        // check that the device has play services available.
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
            applicationContext
        )
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance()
                .getErrorDialog(this, code, RC_HANDLE_GMS)
            dlg!!.show()
        }
        if (mCameraSource != null) {
            try {
                preview!!.start(mCameraSource, mGraphicOverlay)
            } catch (e: IOException) {
                Log.e(
                    TAG,
                    "Unable to start camera source.",
                    e
                )
                mCameraSource?.release()
                mCameraSource = null
            }
        }
    }

    companion object {
        fun launch(context: Context) {
            context.startActivity(Intent(context, EyeActivity::class.java))
        }
    }
}