package com.example.structureapplication.util

object Constants {

    init {
        System.loadLibrary("structureapplication")
    }

    external fun stringFromJNI(): String

    const val BASE_URL = "https://jsonplaceholder.typicode.com/"
//    const val BASE_URL = " https://dms-qas-api.pathmazing.com/"

}